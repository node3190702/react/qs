import React, { FC } from "react";
import styles from './QuestionCard.module.scss'

type ProsType = {
    _id: string,
    title: string,
    isStart: boolean,
    answerCount: number,
    createdAt: string,
    isPublished: boolean,
}

const QuestionCard: FC<ProsType> = (props: ProsType) => {
    const { _id, title, createdAt, answerCount, isPublished } = props
    return (
        <div className={styles.container}>
            <div className={styles.title}>
                <div className={styles.left}>
                    <a href='#'>{title}</a>
                </div>
                <div className={styles.right}>
                    { isPublished? <span style={{color: 'green'}}>已发布</span>: <span>未发布</span> }
                    &npsb;
                    <span>答卷: {answerCount}</span>
                    <span>{createdAt}</span>
                </div>
            </div>
            <div className={styles.buttonContainer}>
                <div className={styles.left}>
                    <button>编辑问卷</button>
                    <button>统计数据</button>
                </div>
                <div className={styles.right}>
                    <button>标星</button>
                    <button>复制</button>
                    <button>删除</button>
                </div>
            </div>
        </div>
    )
}

export default QuestionCard