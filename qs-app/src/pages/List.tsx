import React, { FC, useState } from 'react'
import QuestionCard from "../components/QuestionCard";
import styles from './List.module.scss'

const List: FC = () => {
    const [questionList, setQuestionList] = useState([
        { _id: 'q1', title: '问卷1', isPublished: true, isStart: false, answerCount: 10, createdAt: '2022-01-01' },
        { _id: 'q2', title: '问卷1', isPublished: false, isStart: false, answerCount: 10, createdAt: '2022-01-02' },
        { _id: 'q3', title: '问卷1', isPublished: true, isStart: false, answerCount: 10, createdAt: '2022-01-03' }
    ])
    return (
        <>
            <div className={styles.header}>
                <div className={styles.left}>
                    <h3>我的问卷</h3>
                </div>
                <div className={styles.right}>
                    (搜索)
                </div>
            </div>
            <div className={styles.content}>
                { questionList.map(item => {
                    const { _id } = item
                    return <QuestionCard key={_id} {...item} />
                })}
            </div>
            <div className={styles.footer}>底部</div>
        </>
    )
}

export default List